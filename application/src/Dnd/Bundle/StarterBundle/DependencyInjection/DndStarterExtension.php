<?php

namespace Dnd\Bundle\StarterBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * Class
 *
 * @category        Class
 * @package         Dnd\Bundle\StarterBundle\DependencyInjection
 * @author          Agence Dn'D <contact@dnd.fr>
 * @copyright       2018 Agence Dn'D
 * @license         http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link            http://www.dnd.fr/
 */
class DndStarterExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        die('start');
    }
}
