# > Image start
FROM debian:jessie

# > Contact
LABEL maintainer = "Agence Dn'D <contact@dnd.fr>"

# > Variables
ARG APP_PROJECT_DIR_NAME

# > Volumes
VOLUME /var/www/project

# > Timezone
RUN echo Europe/Paris | tee /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata