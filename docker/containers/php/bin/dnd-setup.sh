#!/usr/bin/env bash

# Launch DnD Setup : Akeneo Starter
echo "Initializing setup..."

# Initialize application directory
cd /var/www/project/akeneo
if [ -d ./vendor ]; then
  echo "It appears Akeneo is already installed (app/etc/config.php or app/etc/env.php exist). Exiting setup..."
  exit
fi

# Setup directory
mkdir /var/www/project/akeneo
chown -R www-data:www-data /var/www/project/*
cd /var/www/project/

# Pull Akeneo
echo "Pulling last stable version of Akeneo ..."
sudo -uwww-data composer create-project --no-install --prefer-dist akeneo/pim-community-standard /var/www/project/tmp "@stable"
o
cp -R /var/www/project/tmp/* /var/www/project/akeneo
rm -rf /var/www/project/tmp/
cd /var/www/project/akeneo

# Install dependencies
echo "Install dependencies ..."
sudo -uwww-data /usr/local/bin/composer install --no-scripts

######################
# > Install Akeneo  ##
######################
cp /var/tmp/parameters.yml.dist /var/www/project/akeneo/app/config/parameters.yml
cp /var/tmp/parameters.yml.dist /var/www/project/akeneo/app/config/parameters_test.yml
cp /var/tmp/parameters.yml.dist /var/www/project/akeneo/app/config/parameters_prod.yml

# Empty cache
sudo -uwww-data rm -rf ./var/cache/*

# Setup directory
chown -R www-data:www-data /var/www/project/*

# Install database
sudo -uwww-data php /var/www/project/akeneo/bin/console pim:install

# Install assets
sudo -uwww-data php /var/www/project/akeneo/bin/console assets:install web
sudo -uwww-data php /var/www/project/akeneo/bin/console assets:install --symlink web
sudo -uwww-data php /var/www/project/akeneo/bin/console pim:installer:assets
sudo -uwww-data yarn run webpack-dev
# < Install Akeneo  ##